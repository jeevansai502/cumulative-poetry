import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class CumulativePoetryTest {
    private CumulativePoetry cumulativePoetry;

    @Before
    public void setUp() {
        cumulativePoetry = new CumulativePoetry();
    }

    @Test
    public void testNoSwitch() {
        String[] emptyArray = {};
        assertEquals("Please provide a switch '--reveal-for-day for_which_day' or '--recite'", cumulativePoetry.execute(emptyArray));
    }

    @Test
    public void testRecite() {
        String[] reciteArray = {"--recite"};
        assertEquals("This is the horse and the hound and the horn that belonged to\nthe farmer sowing his corn that kept\nthe rooster that crowed in the morn that woke\nthe priest all shaven and shorn that married\nthe man all tattered and torn that kissed\nthe maiden all forlorn that milked\nthat cow with the crumpled horn that tossed\nthe dog that worried\nthe cat that killed\nthe rat that ate\nthe malth that lay in\nthe house that Jack built.", cumulativePoetry.execute(reciteArray));
    }

    @Test
    public void testPoetryForDay() {

        ArrayList<String> poetry = new ArrayList<String>( Arrays.asList("This is the horse and the hound and the horn that belonged to",
                "the farmer sowing his corn that kept",
                "the rooster that crowed in the morn that woke",
                "the priest all shaven and shorn that married",
                "the man all tattered and torn that kissed",
                "the maiden all forlorn that milked",
                "that cow with the crumpled horn that tossed",
                "the dog that worried",
                "the cat that killed",
                "the rat that ate",
                "the malth that lay in",
                "the house that Jack built.") );

        assertEquals("the house that Jack built.", cumulativePoetry.poetryForDay(1));
        assertEquals("the rat that ate\nthe malth that lay in\nthe house that Jack built.", cumulativePoetry.poetryForDay(3));

    }

    @Test
    public void testPoetryWrongInput() {
        String[] revealArray = {"--reveal-for-day" ,"0"};
        assertEquals("Sorry, poetry doesn't exist for that day", cumulativePoetry.execute( revealArray ));
        revealArray[1] = "13";
        assertEquals("Sorry, poetry doesn't exist for that day", cumulativePoetry.execute( revealArray ));
        revealArray[1] = "y";
        assertEquals("Please check your input", cumulativePoetry.execute(revealArray));
    }


}
