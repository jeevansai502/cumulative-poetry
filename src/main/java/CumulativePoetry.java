import java.util.*;

public class CumulativePoetry {

    ArrayList<String> poetry = new ArrayList<String>( Arrays.asList("This is the horse and the hound and the horn that belonged to",
            "the farmer sowing his corn that kept",
            "the rooster that crowed in the morn that woke",
            "the priest all shaven and shorn that married",
            "the man all tattered and torn that kissed",
            "the maiden all forlorn that milked",
            "that cow with the crumpled horn that tossed",
            "the dog that worried",
            "the cat that killed",
            "the rat that ate",
            "the malth that lay in",
            "the house that Jack built.") );


    String poetryForDay(int day) {

        return String.join("\n", this.poetry.subList(poetry.size()-day, poetry.size()));
    }

    String recite() {
        return String.join("\n", this.poetry);
    }

    String execute(String[] args) {

        if (args.length > 0) {

            if (args.length > 1) {
                if (args[0].equals("--reveal-for-day")) {

                    try {
                        if ( Integer.parseInt(args[1]) > 0 && Integer.parseInt(args[1]) <= 12 ){
                            return this.poetryForDay(Integer.parseInt(args[1])) ;
                        }else {
                            return "Sorry, poetry doesn't exist for that day";
                        }
                    }catch(Exception ex) {
                        return "Please check your input";
                    }

                }else {
                    return "Please provide correct switches";
                }
            }else {
                if (args[0].equals("--recite")) {
                    return this.recite();
                }else {
                    return "Please provide correct switches";
                }
            }

        }else {
            return "Please provide a switch '--reveal-for-day for_which_day' or '--recite'";
        }
    }

    public static void main(String[] args) {
        CumulativePoetry cumulativePoetry = new CumulativePoetry();
        System.out.println(cumulativePoetry.execute(args));
    }
}
